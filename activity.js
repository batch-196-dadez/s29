db = db.getSiblingDB("QueryDB");
db.getCollection("courses").find({});

db.courses.insertMany(
	[

	{
    "name" : "HTML Basics",
    "price" : 2000.0,
    "isActive" : true,
    "instructor" : "Sir Alvin",
    "_id" : ObjectId("62e0fcc1d86ec22c695129f2")
},
// Newly added document
{
    "name" : "CSS 101 + Flexbox",
    "price" : 21000.0,
    "isActive" : true,
    "instructor" : "Sir Alvin",
    "_id" : ObjectId("62e0fd33d86ec22c695129f3")
},
// Newly added document
{
    "name" : "Javascript 101",
    "price" : 32000.0,
    "isActive" : true,
    "instructor" : "Ma'am Tine",
    "_id" : ObjectId("62e0fd67d86ec22c695129f4")
},
// Newly added document
{
    "name" : "GIT 101, IDE and CLI",
    "price" : 19000.0,
    "isActive" : false,
    "instructor" : "Ma'am Tine",
    "_id" : ObjectId("62e0fda0d86ec22c695129f5")
},
// Newly added document
{
    "name" : "React.Js 101",
    "price" : 25000.0,
    "isActive" : true,
    "instructor" : "Ma'am Miah",
    "_id" : ObjectId("62e0fdc9d86ec22c695129f6")
}



	])

db.courses.find({$and:[{instructor:{$regex: 'Sir Alvin',$options: '$i'}},{price:{$lte:20000}}]});

db.courses.find({$and:[{instructor:{$regex: 'Maam Tine',$options: '$i'}},{isActive:{$gte:false}}]});

db.courses.find({$and:[{name:{$regex: 'r',$options: '$i'}},{price:{$gte:25000}}]});


db.courses.updateMany({price.{$lt:21000}},{set:isActive:false})
db.courses.deleteMny({price.{$lt:25000})